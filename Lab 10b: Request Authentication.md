In this lab exercise we will look at enabling, configuring, and using Request Authentication policies.  
The Request Authentication policy can be defined in global mesh level in the root namespace istio-system or applied to specific CNF Producer workloads with selector label in a CNF Producer namespace. The Producer workload performs Request Authentication can be ordinary sidecars or Istio ingress gateways.
Create sample Consumer and Producer apps (optional if existing):
### The steps below is lab preparation steps for 10b.1 and 10b.2 

```
$ cd ~/am-training/samples/generic-nf2nf-scenarios

Verify the results:
$ oc get all -n nf2nf-h2load
$ oc get all -n nf2nf-nginx

Send a test request:
$ oc exec $(oc get pod -n nf2nf-h2load -l app=h2load -o jsonpath={.items..metadata.name}) -c h2load -n nf2nf-h2load -- curl  -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200” response code.



```
### (The steps are already performed) 
```
Steps:
$./setup.sh <test-case> <install-flag>

For example:
$./setup.sh 4s-4s -I

Verify the results:
$ oc get all -n nf2nf-h2load
$ oc get all -n nf2nf-nginx

Send a test request:
$ oc exec $(oc get pod -n nf2nf-h2load -l app=h2load -o jsonpath={.items..metadata.name}) -c h2load -n nf2nf-h2load -- curl  -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200” response code.

```

### Notes:

- This will create 2 sample CNFs (h2load, nginx, etc) in different namespaces (nf2nf-h2load, nf2nf-nginx) if they do not exist. The CNF Producer must have sidecar injected for this lab.

- The sample CNF Consumer service in nf2nf-h2load namespace will request “nginx.nf2nf-nginx.svc.cluster.local” service in nf2nf-nginx namespace in above example, both CNF Consumer and Producer services are within the same mesh without  Gateways in the path to egress and ingress the traffic.

- The <test-case> command line parameter needs to follow certain format: "4", "6", and "d", which indicates if the CNF Consumer or Producer Services will be single stack (ipv4 or ipv6) or dual stack mode. The "s" indicates sidecar injection enabled. The above sample "4s-ds" means CNF Consumer is a single stack ipv6 service and the CNF Producer is a dual stack ipv6 preferred service. The script will trigger the configure changes (ipFamilies and ipFamilyPolicy) to the "Service" object according to this <test-case> value for different scenarios.

- The <install-flag> can be “-I” for install or “-U” for uninstall the services.


## Start Lab here

| Case Number | Name |
| ------ | ------ |
|   10b.1     |    Enabled Request Authentication Policy for Workload (JWKS)    |
|    10b.2    |    Enabled Request Authentication Policy for Workload (URI)    |


### Lab 10b.1: Enabled Request Authentication Policy for Workload (JWKS)
This example demonstrates Request Authentication with a pair of Consumer and Producer services with JWT approach, with the key sets defined directly in the policy in JSON format.

1.	Verify the Request Authentication policy definition for Producer “nginx” workload. To reject requests without valid tokens, add an Authorization Policy with a rule specifying a DENY action for requests without request principals, shown as notRequestPrincipals: ["*"] in the following example. Request principals are available only when valid JWT tokens are provided. The rule therefore denies requests without valid tokens:

```

$ cd ~/am-training/samples/generic-reqs-authn-scenarios
$ vi yaml/reqauth_jwks.yaml
apiVersion: "security.istio.io/v1beta1"
kind: "RequestAuthentication"
metadata:
  name: "jwks-example"
spec:
  selector:
    matchLabels:
      app: nginx
  jwtRules:
  - issuer: "example-issuer@insecure-examples.aspenmesh.io"
    jwks: |
      ${JWKS}
    forwardOriginalToken: true
…
$ vi yaml/authz_policy.yaml
apiVersion: "security.istio.io/v1beta1"
kind: "AuthorizationPolicy"
metadata:
  name: authz-policy
spec:
  selector:
    matchLabels:
      app: nginx
  action: DENY
  rules:
  - from:
    - source:
        notRequestPrincipals: ["*"]

Modify the definition if needed and save change if any.

```

2.	Create the Request Authentication policy:

```

$ cd ~/am-training/samples/generic-reqs-authn-scenarios
$ ./setup.sh <producer-ns> <jwk-flag> <install-flag>

For example:
$ ./setup.sh nf2nf-nginx -L -I

Verify the AuthN policy created:
$ oc get requestauthentication jwks-example -n nf2nf-nginx -o yaml

apiVersion: security.istio.io/v1beta1
kind: RequestAuthentication
metadata:
  name: jwks-example
  namespace: nf2nf-nginx
spec:
  jwtRules:
  - forwardOriginalToken: true
    issuer: example-issuer@insecure-examples.aspenmesh.io
    jwks: |
      {  "keys": [ {    "kty": "EC",    "crv": "P-256",    "kid": "1",    "x": "ZNxpH7OQXXn91yM5nVFrKiVb2jVz5DB0HDPBaWJVTPg=",    "y": "59NctufU04y1qMLGOf2gCM-0a0c80K-eHwSZ9OhJHqE="  } ]}
  selector:
    matchLabels:
      app: nginx

$ oc get authorizationpolicy authz-policy -n nf2nf-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: authz-policy
  namespace: nf2nf-nginx
spec:
  action: DENY
  rules:
  - from:
    - source:
        notRequestPrincipals:
        - '*'
  selector:
    matchLabels:
      app: nginx

```


### Notes:

- The setup.sh script "install-flag" command line parameter can be "-I" for creating the request authentication policy to the Producer namespace, while the "-U" will delete the request authentication policy from the Producer namespace

- The setup.sh script "jwk-flag" command line parameter can be "-U" which uses Uri to access public keys, while "-L" option for embedding a list of public keys in JSON format in the authN policy definition directly.
- The seup.sh script "producer-namespace" command line parameter will only be the destination CNF service namespace which authenticates the request from CNF Consumer JWT in request with the defined request authN policy. 


#### 3. Verify if the policy can authenticate JWT in request:

```

$ cd ~/am-training/samples/generic-reqs-authn-scenarios
$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge -H "Authorization: Bearer $(cat jwt/ec-jwt.txt)" http://nginx.nf2nf-nginx.svc.cluster.local:80/

The ec-jwt.txt is a sample valid JWT, the result should expect “200” with the token passed to the CNF Producer:

* Connected to nginx.nf2nf-nginx.svc.cluster.local (10.121.45.110) port 80 (#0)
* Using HTTP2, server supports multiplexing
* Connection state changed (HTTP/2 confirmed)
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Using Stream ID: 1 (easy handle 0x561418dfee80)
> GET / HTTP/2
> Host: nginx.nf2nf-nginx.svc.cluster.local
> user-agent: curl/7.81.0
> accept: */*
> authorization: Bearer eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEifQ.eyJzdWIiOiJleGFtcGxlLXN1YmplY3RAaW5zZWN1cmUtZXhhbXBsZXMuYXNwZW5tZXNoLmlvIiwiaXNzIjoiZXhhbXBsZS1pc3N1ZXJAaW5zZWN1cmUtZXhhbXBsZXMuYXNwZW5tZXNoLmlvIiwiaWF0IjoxNTMyMzg5NzAwLCJleHAiOjQ2ODU5ODk3MDB9.NfH0zFYDtRqLHX8zQ1FqPNnNeUlLZdPM1Tg8AtmUoZBy8KLr7z3mVeDTYUfsbYIOA4GLPEZHhkz4STf-UkzfPg
>
* Connection state changed (MAX_CONCURRENT_STREAMS == 2147483647)!
< HTTP/2 200

```

4.	Test with a forged JWT token:

```

$ cd ~/am-training/samples/generic-reqs-authn-scenarios
$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge -H "Authorization: Bearer $(cat jwt/ec-jwt-forged.txt)" http://nginx.nf2nf-nginx.svc.cluster.local:80/

The ec-jwt-forged.txt is a sample invalid JWT, the result should expect “401” with the token passed to the CNF Producer:

* Connected to nginx.nf2nf-nginx.svc.cluster.local (10.121.45.110) port 80 (#0)
* Using HTTP2, server supports multiplexing
* Connection state changed (HTTP/2 confirmed)
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Using Stream ID: 1 (easy handle 0x5568f84b4e80)
> GET / HTTP/2
> Host: nginx.nf2nf-nginx.svc.cluster.local
> user-agent: curl/7.81.0
> accept: */*
> authorization: Bearer eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEifQ.eyJzdWIiOiJleGFtcGxlLXN1YmplY3RAaW5zZWN1cmUtZXhhbXBsZXMuYXNwZW5tZXNoLmlvIiwiaXNzIjoiZXhhbXBsZS1pc3N1ZXJAaW5zZWN1cmUtZXhhbXBsZXMuYXNwZW5tZXNoLmlvIiwiaWF0IjoxNTMyMzg5NzAwLCJleHAiOjQ2ODU5ODk3MDB9.GMg2VYM1XT0SzCez4DuCXD1q88AuAkn1SOdUdIEDO4eneBo34riRwj0xryMyIZCEINnbQBuIgLZxAktGXRFY1w
>
* Connection state changed (MAX_CONCURRENT_STREAMS == 2147483647)!
< HTTP/2 401

```

5. Test with faked JWT:

```

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge -H "Authorization: Bearer faked" http://nginx.nf2nf-nginx.svc.cluster.local:80/

The result should expect “401” without a token passed to the CNF Producer:

* Connected to nginx.nf2nf-nginx.svc.cluster.local (10.121.45.110) port 80 (#0)
* Using HTTP2, server supports multiplexing
* Connection state changed (HTTP/2 confirmed)
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Using Stream ID: 1 (easy handle 0x5557f2c6be80)
> GET / HTTP/2
> Host: nginx.nf2nf-nginx.svc.cluster.local
> user-agent: curl/7.81.0
> accept: */*
> authorization: faked
>
* Connection state changed (MAX_CONCURRENT_STREAMS == 2147483647)!
< HTTP/2 401

```

### Lab 10b.2: Enabled Request Authentication Policy for Workload (URI)

This example demonstrates Request Authentication with a pair of Consumer and Producer services with JWT approach, with the key sets in JSON format retrieved from an accessible URL in the policy.

1.	Verify the Request Authentication policy definition for Producer “nginx” workload. To reject requests without valid tokens, add an Authorization Policy with a rule specifying a DENY action for requests without request principals, shown as notRequestPrincipals: ["*"] in the following example. Request principals are available only when valid JWT tokens are provided. The rule therefore denies requests without valid tokens:

```

$ cd ~/am-training/samples/generic-reqs-authn-scenarios
$ vi yaml/reqauth_jwkuri.yaml
apiVersion: "security.istio.io/v1beta1"
kind: "RequestAuthentication"
metadata:
  name: "jwkuri-example"
spec:
  selector:
    matchLabels:
      app: nginx
  jwtRules:
  - issuer: "testing@secure.istio.io"
    jwksUri: "https://raw.githubusercontent.com/istio/istio/release-1.8/security/tools/jwt/samples/jwks.json"
    forwardOriginalToken: true
…
Modify the definition if needed and save change if any.

```

2.	Create the Request Authentication policy:

```

$ cd ~/am-training/samples/generic-reqs-authn-scenarios
$ ./setup.sh <producer-ns> <jwk-flag> <install-flag>

For example:
$ ./setup.sh nf2nf-nginx -U -I

Verify the AuthN policy created:
$ oc get requestauthentication jwkuri-example -n nf2nf-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: RequestAuthentication
metadata:
  name: jwkuri-example
  namespace: nf2nf-nginx
spec:
  jwtRules:
  - forwardOriginalToken: true
    issuer: testing@secure.istio.io
    jwksUri: https://raw.githubusercontent.com/istio/istio/release-1.8/security/tools/jwt/samples/jwks.json
  selector:
    matchLabels:
      app: nginx

Note: The URL https://raw.githubusercontent.com/istio/istio/release-1.8/security/tools/jwt/samples/jwks.json provides the following key set in JSON format:
      
{ "keys":[ {"e":"AQAB","kid":"DHFbpoIUqrY8t2zpA2qXfCmr5VO5ZEr4RzHU_-envvQ","kty":"RSA","n":"xAE7eB6qugXyCAG3yhh7pkDkT65pHymX-P7KfIupjf59vsdo91bSP9C8H07pSAGQO1MV_xFj9VswgsCg4R6otmg5PV2He95lZdHtOcU5DXIg_pbhLdKXbi66GlVeK6ABZOUW3WYtnNHD-91gVuoeJT_DwtGGcp4ignkgXfkiEm4sw-4sfb4qdt5oLbyVpmW6x9cfa7vs2WTfURiCrBoUqgBo_-4WTiULmmHSGZHOjzwa8WtrtOQGsAFjIbno85jp6MnGGGZPYZbDAa_b3y5u-YpW7ypZrvD8BgtKVjgtQgZhLAGezMt0ua3DRrWnKqTZ0BJ_EyxOGuHJrLsn00fnMQ"}]}

$ oc get authorizationpolicy authz-policy -n nf2nf-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: authz-policy
  namespace: nf2nf-nginx
spec:
  action: DENY
  rules:
  - from:
    - source:
        notRequestPrincipals:
        - '*'
  selector:
    matchLabels:
      app: nginx

```

3.	Verify if the policy can authenticate JWT in request:

```

$ cd ~/am-training/samples/generic-reqs-authn-scenarios
$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge -H "Authorization: Bearer $(cat jwt/ec-jwturi.txt)" http://nginx.nf2nf-nginx.svc.cluster.local:80/

The ec-jwturi.txt is a sample valid JWT from https://raw.githubusercontent.com/istio/istio/release-1.8/security/tools/jwt/samples/demo.jwt, the result should expect “200” with the token passed to the CNF Producer:

* Connected to nginx.nf2nf-nginx.svc.cluster.local (10.121.45.110) port 80 (#0)
* Using HTTP2, server supports multiplexing
* Connection state changed (HTTP/2 confirmed)
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Using Stream ID: 1 (easy handle 0x55b8fec56e80)
> GET / HTTP/2
> Host: nginx.nf2nf-nginx.svc.cluster.local
> user-agent: curl/7.81.0
> accept: */*
> authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkRIRmJwb0lVcXJZOHQyenBBMnFYZkNtcjVWTzVaRXI0UnpIVV8tZW52dlEiLCJ0eXAiOiJKV1QifQ.eyJleHAiOjQ2ODU5ODk3MDAsImZvbyI6ImJhciIsImlhdCI6MTUzMjM4OTcwMCwiaXNzIjoidGVzdGluZ0BzZWN1cmUuaXN0aW8uaW8iLCJzdWIiOiJ0ZXN0aW5nQHNlY3VyZS5pc3Rpby5pbyJ9.CfNnxWP2tcnR9q0vxyxweaF3ovQYHYZl82hAUsn21bwQd9zP7c-LS9qd_vpdLG4Tn1A15NxfCjp5f7QNBUo-KC9PJqYpgGbaXhaGx7bEdFWjcwv3nZzvc7M__ZpaCERdwU7igUmJqYGBYQ51vr2njU9ZimyKkfDe3axcyiBZde7G6dabliUosJvvKOPcKIWPccCgefSj_GNfwIip3-SsFdlR7BtbVUcqR-yv-XOxJ3Uc1MI0tz3uMiiZcyPV7sNCU4KRnemRIMHVOfuvHsU60_GhGbiSFzgPTAa9WTltbnarTbxudb_YEOx12JiwYToeX0DCPb43W1tzIBxgm8NxUg
>
* Connection state changed (MAX_CONCURRENT_STREAMS == 2147483647)!
< HTTP/2 200


```

4.	Test without a JWT:

```

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

The result should expect “403” without a token passed to the CNF Producer:

* Connected to nginx.nf2nf-nginx.svc.cluster.local (10.121.45.110) port 80 (#0)
* Using HTTP2, server supports multiplexing
* Connection state changed (HTTP/2 confirmed)
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Using Stream ID: 1 (easy handle 0x5557f2c6be80)
> GET / HTTP/2
> Host: nginx.nf2nf-nginx.svc.cluster.local
> user-agent: curl/7.81.0
> accept: */*
> authorization: faked
>
* Connection state changed (MAX_CONCURRENT_STREAMS == 2147483647)!
< HTTP/2 403

```

5.	Test with a forged JWT token: 

```

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge -H "Authorization: Bearer $(cat jwt/ec-jwt-forged.txt)" http://nginx.nf2nf-nginx.svc.cluster.local:80/

The ec-jwt-forged.txt is a sample invalid JWT, the result should expect “401” with the token passed to the CNF Producer:

* Connected to nginx.nf2nf-nginx.svc.cluster.local (10.121.45.110) port 80 (#0)
* Using HTTP2, server supports multiplexing
* Connection state changed (HTTP/2 confirmed)
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Using Stream ID: 1 (easy handle 0x5568f84b4e80)
> GET / HTTP/2
> Host: nginx.nf2nf-nginx.svc.cluster.local
> user-agent: curl/7.81.0
> accept: */*
> authorization: Bearer eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEifQ.eyJzdWIiOiJleGFtcGxlLXN1YmplY3RAaW5zZWN1cmUtZXhhbXBsZXMuYXNwZW5tZXNoLmlvIiwiaXNzIjoiZXhhbXBsZS1pc3N1ZXJAaW5zZWN1cmUtZXhhbXBsZXMuYXNwZW5tZXNoLmlvIiwiaWF0IjoxNTMyMzg5NzAwLCJleHAiOjQ2ODU5ODk3MDB9.GMg2VYM1XT0SzCez4DuCXD1q88AuAkn1SOdUdIEDO4eneBo34riRwj0xryMyIZCEINnbQBuIgLZxAktGXRFY1w
>
* Connection state changed (MAX_CONCURRENT_STREAMS == 2147483647)!
< HTTP/2 401

```


### CLEAN UP Procedure: 

```

$ cd ~/am-training/samples/generic-reqs-authn-scenarios
./cleanup.sh 

```







