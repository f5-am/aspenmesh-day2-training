### This document provides step by step hands on labs for F5 Aspen Mesh training.

Learn more about F5's Aspen Mesh at [Aspen Mesh Docs](https://clouddocs.f5.com/products/aspen-service-mesh/1.14/)

[Training Slides](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Verizon-Aspen-Mesh-Training-Part2.pdf)


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Hands on Labs**  

- [1. Deploy the lab](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/DeployLab.md?ref_type=heads)

- [2. Lab 03a: Labs Prepares](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Lab-03a_Labs_Prepares.pdf)

- [3. Lab 08a: Ingress gateway use cases](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Lab-08a_Ingress_gateway_use_cases.pdf)

- [4. Lab 09a: ServiceEntry by DNS Controller](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Lab-09a_ServiceEntry_by_DNS_Controller.pdf)

- [5. Lab 10a: Peer Authentication](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Lab-10a_Peer_Authentication.pdf)

- [6. Lab 10b: Request Authentication](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Lab-10b_Request_Authentication.pdf)

- [7. Lab 10c: Authorization Policy](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Lab-10c_Authorization.pdf)

- [8. Lab 15: Packet Capture](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Lab-15_Packet_Capture.pdf)

- [9. Lab 16 OAM](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Lab-16_OAM.pdf)

- [10. Lab Diagram](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/Lab%20PDF/Verizon-AM-Training_LabDiagrams.pdf)



