```

[cloud-user@ocp-provisioner generic-mtls-scenarios]$ ./setup.sh 4s-4s -Y -I
This case1 enables mTLS, with sidecars in both CNF Consumer and CNF Producer.
Creating CNF Consumer service:
Create CNF consumer namespace case1-h2load if not exists...
namespace/case1-h2load created
namespace/case1-h2load labeled
namespace/case1-h2load labeled
namespace/case1-h2load labeled
networkattachmentdefinition.k8s.cni.cncf.io/istio-cni created
clusterrole.rbac.authorization.k8s.io/system:openshift:scc:privileged added: "system:serviceaccounts:case1-h2load"
No resources found in case1-h2load namespace.
namespace/case1-h2load not labeled
networkattachmentdefinition.k8s.cni.cncf.io/istio-cni unchanged
Parameters:h2load,h2load-case1,case1-h2load,true,SingleStack,IPv4
Helm update set values: sidecarInjectAuto=true,service.ipFamilyPolicy=SingleStack,service.ipFamilies[0]=IPv4
Release "h2load-case1" does not exist. Installing it now.
NAME: h2load-case1
LAST DEPLOYED: Tue Oct 24 17:29:54 2023
NAMESPACE: case1-h2load
STATUS: deployed
REVISION: 1
TEST SUITE: None
No resources found in case1-h2load namespace.
sidecar.networking.istio.io/default created

---------------------------------------------------------------

Creating CNF Producer service:
Create CNF producer namespace case1-nginx if not exists...
namespace/case1-nginx created
namespace/case1-nginx labeled
namespace/case1-nginx labeled
namespace/case1-nginx labeled
networkattachmentdefinition.k8s.cni.cncf.io/istio-cni created
clusterrole.rbac.authorization.k8s.io/system:openshift:scc:anyuid added: "system:serviceaccounts:case1-nginx"
No resources found in case1-nginx namespace.
namespace/case1-nginx not labeled
networkattachmentdefinition.k8s.cni.cncf.io/istio-cni unchanged
Parameters:nginx,nginx-case1,case1-nginx,true,SingleStack,IPv4
Helm update set values: sidecarInjectAuto=true,service.ipFamilyPolicy=SingleStack,service.ipFamilies[0]=IPv4
Release "nginx-case1" does not exist. Installing it now.
NAME: nginx-case1
LAST DEPLOYED: Tue Oct 24 17:30:17 2023
NAMESPACE: case1-nginx
STATUS: deployed
REVISION: 1
TEST SUITE: None
No resources found in case1-nginx namespace.
sidecar.networking.istio.io/default created

---------------------------------------------------------------

Deploy or upgrade DR and PA for mTLS case1...
CONSUMER SET_VALUES: istioResources.dr.enabled=true,istioResources.dr.targetMtlsMode=ISTIO_MUTUAL,istioResources.pa.enabled=false
Release "mtls-case1" does not exist. Installing it now.
NAME: mtls-case1
LAST DEPLOYED: Tue Oct 24 17:30:19 2023
NAMESPACE: case1-h2load
STATUS: deployed
REVISION: 1
TEST SUITE: None
PRODUCER SET_VALUES: istioResources.dr.enabled=false,istioResources.pa.receiveMtlsMode=PERMISSIVE,istioResources.pa.enabled=true
Release "mtls-case1" does not exist. Installing it now.
NAME: mtls-case1
LAST DEPLOYED: Tue Oct 24 17:30:19 2023
NAMESPACE: case1-nginx
STATUS: deployed
REVISION: 1
TEST SUITE: None
[cloud-user@ocp-provisioner generic-mtls-scenarios]$

```
