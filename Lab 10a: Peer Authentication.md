## Basic Sanity Check:

```
oc get nodes
oc get ns
oc get pods -n istio-system
oc get pods -n nf2nf-h2load
oc get pods -n nf2nf-nginx
oc get pods -n nf2nf-sleep
oc get pods -n nf2nf-diameter-client
oc get pods -n nf2nf-diameter-server

``` 



## Lab 10a.4: Typical mTLS Use Cases

This lab includes different test cases in typical mTLS settings, with combinations of Consumer/Producer with/without sidecar. 


#### Notes:
(a) Each test case will create a Consumer in “case#-h2load” and a Producer in “case#-nginx” namespace, with different TLS settings, corresponding to the test “case#”.

(b) The sample CNF Consumer service will request nginx.casex-nginx.svc.cluster.local service. CNF Consumer and Producer services are simulated in/out of the mesh based on test cases.


(c) The setup.sh script "test-case" command line parameter needs to follow certain format: "4", "6", and "d", which indicates if the CNF Consumer or Producer services will be single stack (ipv4 or ipv6) or dual stack mode. The "s" indicates sidecar injection enabled. The above sample "6s-ds" means CNF Consumer is a single stack ipv6 service and the CNF Producer is a dual stack ipv6 preferred service. The script will trigger the changes (ipFamilies and ipFamilyPolicy) to the "Service" object according to the "test-case" value.


(d) The setup.sh script "tls-flag" command line parameter indicates the test case enabling TLS connection or not between the CNF Consumer and CNF Producer, either with or without sidecar injected. Value can be "-Y" (enable TLS) or "-N" (disable TLS). This flag only applies to HTTP protocol.


(e) To simplify the labs, all TLS configurations are only at namespace level, overwriting global level default policy.

(f) The setup.sh script “install-flag” command line parameter can be either “-I” for installing the test cases, or “-U” for uninstalling the test cases.


(g) The test.sh script “test-case” command line parameter needs to match to the above (c) for setup.sh, the “tls-case” command line parameter needs to match to the name of the test case name, such as “case1”, etc. The “protocol” command line parameter needs to be only “-H”, indicating HTTP protocol.

| Number | Usecase | mTLS  | DestinationRule | PeerAuth |
| ------ | ------ | ------ | ------| ------| 
|    1    |   4s-4s    | enabled | ISTIO_MUTUAL | PERMISSIVE |
|    2    |   4s-4s     | disabled | disable | PERMISSIVE |
|  3 | 4-4s | enabled | Consumer app directly uses Citadel managed certs mounted on its container, Producer Service port with HTTPS protocol, and with HTTP2 listener enabled in Producer app | STRICT |
|  4 | 4-4s | disabled | Consumer initiates non-TLS connection, Producer Service port with HTTP2 protocol, with HTTP2 listener enabled in Producer app | PERMISSIVE | 
| 5 | 4s-4 | enabled | SIMPLE | STRICT | 
| 6 | 4s-4 | DISABLE | DISABLE | |
| 7 | 4-4  | Enabled by Consumer and Producer | NA  | NA |
| 8| 4-4 | Enabled by Consumer and Producer | NA  | NA |


#### Case 1: 

Both Consumer and Producer are with sidecars, with mTLS enabled with ISTIO_MUTUAL in DestinationRule and PeerAuthentication in PERMISSIVE mode in namespace level:

```

$ cd ~/am-training/samples/generic-mtls-scenarios/
$./setup.sh <test-case> <tls-flag> <install-flag>"

For example:
$ ./setup.sh 4s-4s -Y -I

```
[Expected setup O/P](https://gitlab.com/f5-am/aspenmesh-day2-training/-/blob/master/10a%20Case1.md)

```
Verify the TLS policy created:
$ oc get dr default -n case1-h2load -o yaml
apiVersion: networking.istio.io/v1beta1
kind: DestinationRule
metadata:
  name: default
  namespace: case1-h2load
spec:
  exportTo:
  - .
  host: '*.local'
  trafficPolicy:
    tls:
      mode: ISTIO_MUTUAL

$ oc get peerauthentication default -n case1-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: default
  namespace: case1-nginx
spec:
  mtls:
    mode: PERMISSIVE

```
Testing Case 1 (4s-4s)
```
$ cd ~/am-training/samples/generic-mtls-scenarios/tests
$./test.sh <test-case> <protocol> <tls-case>

For example:
$./tests.sh 4s-4s -H case1

```

The expected response code should be “<HTTP/2 200>”.

```

$istioctl x describe pod -n case1-h2load $(oc get pod -l app=h2load -n case1-h2load -o jsonpath={.items..metadata.name})

--------------------
Service: h2load
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default for "*.local"
   Traffic Policy TLS Mode: ISTIO_MUTUAL


```

```

$ istioctl x describe pod -n case1-nginx $(oc get pod -l app=nginx -n case1-nginx -o jsonpath={.items..metadata.name})

--------------------
Effective PeerAuthentication:
   Workload mTLS mode: PERMISSIVE
Applied PeerAuthentication:
   default.istio-system, default.case1-nginx

```

#### Case 2: 

 Both Consumer and Producer are with sidecars, with 
 
 > TLS:**DISABLED**
> 
>  DestinationRule: **DISABLE**
> 
>  PeerAuthentication in **PERMISSIVE** mode in namespace level:

```

$ cd ~/am-training/samples/generic-mtls-scenarios
$./setup.sh <test-case> <tls-flag> <install-flag>"

For example:
$ ./setup.sh 4s-4s -N -I

Verify the TLS policy created:
$ oc get dr default -n case2-h2load -o yaml
apiVersion: networking.istio.io/v1beta1
kind: DestinationRule
metadata:
  name: default
  namespace: case2-h2load
spec:
  exportTo:
  - .
  host: '*.local'
  trafficPolicy:
    tls:
      mode: DISABLE

$ oc get peerauthentication default -n case2-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: default
  namespace: case2-nginx
spec:
  mtls:
    mode: PERMISSIVE

```
Testing Case 2:

```

$./tests.sh <test-case> <protocol> <tls-case>

Command to run tests

$ cd ~/am-training/samples/generic-mtls-scenarios/tests

$ ./tests.sh 4s-4s -H case2

```

The expected response code should be “200”.

```

$ istioctl x describe pod -n case2-h2load $(oc get pod -l app=h2load -n case2-h2load -o jsonpath={.items..metadata.name})


…
--------------------
Service: h2load
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default for "*.local"
   Traffic Policy TLS Mode: DISABLE



$ istioctl x describe pod -n case2-nginx $(oc get pod -l app=nginx -n case2-nginx -o jsonpath={.items..metadata.name})
…

--------------------
Service: nginx
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default.istio-system for "*.local"
   Traffic Policy TLS Mode: ISTIO_MUTUAL
--------------------
Effective PeerAuthentication:
   Workload mTLS mode: PERMISSIVE
Applied PeerAuthentication:
   default.istio-system, default.case2-nginx

```

#### Case 3: 

>  Consumer has no sidecar, Producer has sidecar, with 
>  **TLS : enabled**
>  Consumer app directly uses Citadel managed certs mounted on its container, 
>  Producer Service port with HTTPS protocol, and with HTTP2 listener enabled in Producer app, 
> Producer PeerAuthentication in **STRICT mode**:


```

$  cd ~/am-training/samples/generic-mtls-scenarios
$ ./setup.sh <test-case> <tls-flag> <install-flag>"

For example:
$ ./setup.sh 4-4s -Y -I

Verify the TLS policy created:
$ oc get peerauthentication default -n case3-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: default
  namespace: case3-nginx
spec:
  mtls:
    mode: STRICT

```
#### Note: Because of UDF performance , wait for 1-2 mins for citadel certificate to be mounted on the container. 

Testing Case 3 

```

$ cd ~/am-training/samples/generic-mtls-scenarios/tests
$./tests.sh <test-case> <protocol> <tls-case>

For example:
$./tests.sh 4-4s -H case3
Or manually run the command from h2load container:
$ curl -vvv https://nginx.case3-nginx.svc.cluster.local:80/ --cert /opt/test_results/cert-chain.pem --key /opt/test_results/key.pem  -k

The expected response code should be “200”. Should be able to view the TLS handshakes in the output, as the h2load app directly performs TLS handshake in this case, with the following mounted TLS cert and key:

$ oc exec $(oc get pod -l app=h2load -n case3-h2load -o jsonpath={.items..metadata.name}) -n case3-h2load -c h2load -- ls /opt/test_results | grep pem

```

```
cert-chain.pem
key.pem
root-cert.pem
```
```

$ istioctl x describe pod -n case3-nginx $(oc get pod -l app=nginx -n case3-nginx -o jsonpath={.items..metadata.name})


…
--------------------
Effective PeerAuthentication:
   Workload mTLS mode: STRICT
Applied PeerAuthentication:
   default.istio-system, default.case3-nginx

```

Note: In case using PERMISSIVE mode in the policy, the annotation needs to be added in the PeerAuthentication definition: http-alpns.aspenmesh.io: "true"




#### Case 4:

>  Consumer has no sidecar, 
>  Producer has sidecar, TLS disabled, 
>  Consumer initiates non-TLS connection, 
>  Producer Service port with HTTP2 protocol, 
>  with HTTP2 listener enabled in Producer app,   PeerAuthentication in PERMISSIVE mode:



```

$ cd ~/am-training/samples/generic-mtls-scenarios/
$./setup.sh <test-case> <tls-flag> <install-flag>"

For example:
$ ./setup.sh 4-4s -N -I

Verify the TLS policy created:
$ oc get peerauthentication default -n case4-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: default
  namespace: case4-nginx
spec:
  mtls:
    mode: PERMISSIVE

$ cd ~/am-training/samples/generic-mtls-scenarios/tests
$./tests.sh <test-case> <protocol> <tls-case>

For example:
$ ./tests.sh 4-4s -H case4
Or manually run the command from h2load container:
$ curl -vvv --http2-prior-knowledge http://nginx.case4-nginx.svc.cluster.local/

The expected response code should be “200”. 

Verify the policy mode at destination:

$ istioctl x describe pod -n case4-nginx $(oc get pod -l app=nginx -n case4-nginx -o jsonpath={.items..metadata.name})
…
--------------------
Effective PeerAuthentication:
   Workload mTLS mode: PERMISSIVE
Applied PeerAuthentication:
   default.istio-system, default.case4-nginx

```

#### Case 5:

Consumer has sidecar, 
Producer has no sidecar, TLS enabled, 
DR with SIMPLE mode, Producer Service port with TCP protocol, and with TLS listener enabled in Producer app:

```

$ cd ~/am-training/samples/generic-mtls-scenarios
$./setup.sh <test-case> <tls-flag> <install-flag>"

For example:
$ ./setup.sh 4s-4 -Y -I

Verify the TLS policy created:
$ oc get dr default -n case5-h2load -o yaml
apiVersion: networking.istio.io/v1beta1
kind: DestinationRule
metadata:
  name: default
  namespace: case5-h2load
spec:
  exportTo:
  - .
  host: '*.local'
  trafficPolicy:
    tls:
      mode: SIMPLE

```
Testing Case 5:

```
$ cd ~/am-training/samples/generic-mtls-scenarios/tests
$./tests.sh <test-case> <protocol> <tls-case>

For example:
$ ./tests.sh 4s-4 -H case5
Or manually run the command from h2load container:
$ curl -vvv http://nginx.case5-nginx.svc.cluster.local/

The expected response code should be “200”. 

Verify the policy mode at Consumer:

$ istioctl x describe pod -n case5-h2load $(oc get pod -l app=h2load -n case5-h2load -o jsonpath={.items..metadata.name})
…
--------------------
Service: h2load
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default for "*.local"
   Traffic Policy TLS Mode: SIMPLE

Verify the cert and key mounted on Producer container directly:
$ oc exec $(oc get pod -l app=nginx -n case5-nginx -o jsonpath={.items..metadata.name}) -n case5-nginx -c nginx -- ls /etc/nginx-tls/ | grep pem
```
```
cert-chain.pem
key.pem
root-cert.pem

```

#### Case 6:

Consumer has sidecar, Producer has no sidecar, TLS disabled with DR in DISABLE mode, Producer Service port with HTTP2 protocol, and with HTTP2 listener enabled in Producer app:

```

$ cd ~/am-training/samples/generic-mtls-scenarios
$./setup.sh <test-case> <tls-flag> <install-flag>"

For example:
$ ./setup.sh 4s-4 -N -I

Verify the TLS policy created:
$ oc get dr default -n case6-h2load -o yaml
apiVersion: networking.istio.io/v1beta1
kind: DestinationRule
metadata:
  name: default
  namespace: case6-h2load
spec:
  exportTo:
  - .
  host: '*.local'
  trafficPolicy:
    tls:
      mode: DISABLE

```
Testing Case 6:
```

$  cd ~/am-training/samples/generic-mtls-scenarios/tests
$ ./tests.sh <test-case> <protocol> <tls-case>

For example:
$ ./tests.sh 4s-4 -H case6

Or manually run the command from h2load container:
$ curl -vvv --http2-prior-knowledge http://nginx.case6-nginx.svc.cluster.local/

```
The expected response code should be “200”. 

```
Verify the policy mode at Consumer:

$ istioctl x describe pod -n case6-h2load $(oc get pod -l app=h2load -n case6-h2load -o jsonpath={.items..metadata.name})

…
--------------------
Service: h2load
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default for "*.local"
   Traffic Policy TLS Mode: DISABLE

--------------------
Effective PeerAuthentication:
   Workload mTLS mode: STRICT
Applied PeerAuthentication:
   default.istio-system

```

#### Case 7:

Consumer and Producer both have no sidecar, TLS enabled and handled by Consumer and Producer apps directly, Producer Service port with HTTPS protocol, and with TLS listener enabled in Producer app:

```

$ cd ~/am-training/samples/generic-mtls-scenarios
$./setup.sh <test-case> <tls-flag> <install-flag>"

For example:
$ ./setup.sh 4-4 -Y -I

```

```
$ cd ~/am-training/samples/generic-mtls-scenarios/tests
$ ./tests.sh <test-case> <protocol> <tls-case>

For example:

$ ./tests.sh 4-4 -H case7

Or manually run the command from h2load container:
$ curl -vvv https://nginx.case7-nginx.svc.cluster.local:80/ --cert /opt/test_results/cert-chain.pem --key /opt/test_results/key.pem  -k

The expected response code should be “200”. 

Verify the cert and key (managed by Citadel in k8 secrets) mounted on Consumer and Producer apps containers:

$ oc exec $(oc get pod -l app=h2load -n case7-h2load -o jsonpath={.items..metadata.name}) -n case7-h2load -c h2load -- ls /opt/test_results | grep pem

```
cert-chain.pem
key.pem
root-cert.pem
```
$ oc exec $(oc get pod -l app=nginx -n case7-nginx -o jsonpath={.items..metadata.name}) -n case5-nginx -c nginx -- ls /etc/nginx-tls/ | grep pem

```
```
cert-chain.pem
key.pem
root-cert.pem

```

#### Case 8:

 Consumer and Producer both have no sidecar, 
 TLS disabled, 
 Producer Service port with HTTP2 protocol, and with HTTP2 listener enabled in Producer app:


```

$ cd ~/am-training/samples/generic-mtls-scenarios/
$./setup.sh <test-case> <tls-flag> <install-flag>"

For example:
$ ./setup.sh 4-4 -N -I

$ cd ~/am-training/samples/generic-mtls-scenarios/tests
$./tests.sh <test-case> <protocol> <tls-case>

For example:
$./tests.sh 4-4 -H case8


Or manually run the command from h2load container:
$ $ curl -vvv --http2-prior-knowledge http://nginx.case8-nginx.svc.cluster.local/

The expected response code should be “200”. 

```



### Clean Up of the labs:

All the labs can be cleaned up by using the script below:

```
$ cd ~/am-training/samples/generic-mtls-scenarios/
$ ./cleanup.sh

```



