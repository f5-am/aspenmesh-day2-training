### In this lab exercise we will look at enabling, configuring, and using Peer Authentication policies. 

Create sample Consumer and Producer apps (optional if existing):
```
$ cd ~samples/generic-nf2nf-scenarios
$./setup.sh <test-case> <install-flag>

For example:
$./setup.sh 4s-ds -I

Verify the results:
$ oc get all -n nf2nf-h2load
$ oc get all -n nf2nf-nginx

Send a test request:
$ oc exec $(oc get pod -n nf2nf-h2load -l app=h2load -o jsonpath={.items..metadata.name}) -c h2load -n nf2nf-h2load -- curl  -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200” response code.

```

### Notes:
1. This will create 2 sample CNFs (h2load, nginx, etc.) in different namespaces (nf2nf-h2load, nf2nf-nginx) if they do not exist. The CNF Consumer and Producer both must have sidecar injected in this lab.
2. The sample CNF Consumer service in nf2nf-h2load namespace will request “nginx.nf2nf-nginx.svc.cluster.local” service in nf2nf-nginx namespace in above example, both CNF Consumer and Producer services are within the same mesh without  Gateways in the path to egress and ingress the traffic.
3. The <test-case> command line parameter needs to follow certain format: "4", "6", and "d", which indicates if the CNF Consumer or Producer Services will be single stack (ipv4 or ipv6) or dual stack mode. The "s" indicates sidecar injection enabled. The above sample "4s-ds" means CNF Consumer is a single stack ipv6 service and the CNF Producer is a dual stack ipv6 preferred service. The script will trigger the configure changes (ipFamilies and ipFamilyPolicy) to the "Service" object according to this <test-case> value for different scenarios.
(d) The <install-flag> can be “-I” for install or “-U” for uninstall the services.


### Lab 10a.1: Globally Enabled mTLS 

Aspen Mesh by default enables mutual TLS on all the sidecar proxies of the workloads in the mesh. 

During Aspen Mesh Control Plane installation, the mesh-wide Peer Authentication policy is defined in the Control Plane namespace istio-system. Inspect the default Peer Authentication policy, and the TLS settings in Consumer/Producer sidecar configures:

```

$ oc get dr default -n istio-system -o yaml

apiVersion: networking.istio.io/v1beta1
kind: DestinationRule
metadata:
…
  name: default
  namespace: istio-system
spec:
  host: '*.local'
  trafficPolicy:
    tls:
      mode: ISTIO_MUTUAL

$ oc get peerauthentication -n istio-system default -o yaml

apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
…
  name: default
  namespace: istio-system
spec:
  mtls:
    mode: STRICT

```

Verify the global TLS settings in Consumer and Producer sidecar configurations:

```
$ istioctl x describe pod -n nf2nf-h2load $(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})
…
Pod: h2load-784d9dc5d9-9xhqg
…
--------------------
Service: h2load
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default.istio-system for "*.local"
   Traffic Policy TLS Mode: ISTIO_MUTUAL
…

$ istioctl x describe pod -n nf2nf-nginx $(oc get pod -l app=nginx -n nf2nf-nginx -o jsonpath={.items..metadata.name})   

Pod: nginx-76c4c9fd97-gc4wz
…
--------------------
Effective PeerAuthentication:
   Workload mTLS mode: STRICT
Applied PeerAuthentication:
   default.istio-system

```

### Lab 10a.2: Namespace Level Enabled mTLS 

The namespace level TLS policies overwrites the mesh global TLS policies, on the sidecar proxies of the workloads in a namespace. 
1.	Create namespace level DestinationRule in Consumer namespace and Peerauthentication policy in Producer namespace, to overwrite the global level policy:

```
$ cd ~samples/generic-peer-authn-scenarios
$ vi values.yaml
…
ns_wide_mtls:
  dr:
    enabled: true
    # The targetMtlsMode can be: DISABLE, SIMPLE, MUTUAL, ISTIO_MUTUAL
    targetMtlsMode: DISABLE
    targetDestHost: "*.local"
  pa:
    enabled: true
    # The receiveMtlsMode can be: STRICT, PERMISSIVE
    receiveMtlsMode: PERMISSIVE
…
Save change if any.
$ ./setup.sh <consumer-ns> <producer-ns> <mtls-level> <install-flag>

For example:
$ ./setup.sh nf2nf-h2load nf2nf-nginx -N -I

```

2.	Verify the namespace level TLS policy created:

```
$ oc get dr default -n nf2nf-h2load -o yaml
apiVersion: networking.istio.io/v1beta1
kind: DestinationRule
metadata:
  name: default
  namespace: nf2nf-h2load
spec:
  exportTo:
  - .
  host: '*.local'
  trafficPolicy:
    tls:
      mode: DISABLE

$ oc get peerauthentication default -n nf2nf-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: default
  namespace: nf2nf-nginx
spec:
  mtls:
    mode: PERMISSIVE

```


Notes: 
(a)  "dr" set to "true", the setup.sh script will deploy the mTLS DestinationRule in Consumer namespace, otherwise the existing mTLS DestinationRule will remain unchanged.
(b) If "pa" set to "true", the setup.sh script will deploy the PeerAuthentication policy in Producer namespace, otherwise the existing mTLS policy will remain unchanged.
(c) The <mtls-level>can be “-N” for namespace level, “-S” for workload level, and “-P” for port level.

3.	Check Consumer and Producer sidecars TLS settings:

```
Send a test request:
$ oc exec $(oc get pod -n nf2nf-h2load -l app=h2load -o jsonpath={.items..metadata.name}) -c h2load -n nf2nf-h2load -- curl  -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200” response code.

Inspect the mTLS settings in the Consumer and Producer sidecars, set by namespace level TLS policy:
$ istioctl x describe pod -n nf2nf-h2load $(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})
…
Pod: h2load-784d9dc5d9-9xhqg
…
--------------------
Service: h2load
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default for "*.local"
   Traffic Policy TLS Mode: DISABLE
…

$ istioctl x describe pod -n nf2nf-nginx $(oc get pod -l app=nginx -n nf2nf-nginx -o jsonpath={.items..metadata.name})   

Pod: nginx-76c4c9fd97-gc4wz
…
--------------------
Effective PeerAuthentication:
   Workload mTLS mode: PERMISSIVE
Applied PeerAuthentication:
   default.istio-system, default.nf2nf-nginx

```

### Lab 10a.3: Workload Level Enabled mTLS 
The workload level TLS policies overwrite the mesh global and namespace level TLS policies, on the sidecar proxies of the selected workloads in a namespace. 
1.	Create workload level DestinationRule in Consumer namespace and Peerauthentication policy in Producer namespace, to overwrite the global level and namespace level policies:

```
$ cd ~samples/generic-peer-authn-scenarios
$ vi values.yaml
…
service_level_mtls:
  dr:
    enabled: true
    # The targetMtlsMode can be: DISABLE, SIMPLE, MUTUAL, ISTIO_MUTUAL
    targetMtlsMode: ISTIO_MUTUAL
    # Can use istio.<client sa> such as istio.h2load
    clientCredential:
    targetDestHost: "nginx.nf2nf-nginx.svc.cluster.local"
    # CNF server workload label
    targetServiceName: nginx
    client_labels:
      app: h2load
  pa:
    enabled: true
    # The receiveMtlsMode can be: STRICT, PERMISSIVE
    receiveMtlsMode: STRICT
    # CNF server workload label
    receiveServiceName: nginx
    server_labels:
      app: nginx
…
Save change if any.
$ ./setup.sh <consumer-ns> <producer-ns> <mtls-level> <install-flag>

For example:
$ ./setup.sh nf2nf-h2load nf2nf-nginx -S -I

```

Note: The workload level policy here intends to enable the TLS again.

2.	Verify the workload level overwritten TLS policy created, notice the “selector” or the ”workloadSelector” use “matchLabels” to restrict the policy only applying to the matched workload in the namespaces, other workload in the namespace will continue using the namespace level or global TLS policies:

```
$ oc get dr nginx-svc-dr -n nf2nf-h2load -o yaml
apiVersion: networking.istio.io/v1beta1
kind: DestinationRule
metadata:
  name: nginx-svc-dr
  namespace: nf2nf-h2load
spec:
  exportTo:
  - .
  host: 'nginx.nf2nf-nginx.svc.cluster.local'
  trafficPolicy:
    tls:
      mode: DISABLE
  workloadSelector:
    matchLabels:
      app: h2load

$ oc get peerauthentication nginx-svc-policy -n nf2nf-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: nginx-svc-policy
  namespace: nf2nf-nginx
spec:
  mtls:
    mode: STRICT
  selector:
    matchLabels:
      app: nginx

```

3.	Check Consumer and Producer sidecars TLS settings for the specified service:

```

Send a test request:
$ oc exec $(oc get pod -n nf2nf-h2load -l app=h2load -o jsonpath={.items..metadata.name}) -c h2load -n nf2nf-h2load -- curl  -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “503” response code, as the “DIABLE” mode in DR would only work with “PERMISSIVE” mode PA policy.
```

Inspect the mTLS settings in the Consumer and Producer sidecars, set by workload level TLS policy:

```
$ istioctl x describe pod -n nf2nf-h2load $(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})
…
Pod: h2load-784d9dc5d9-9xhqg
…
--------------------
Service: h2load
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default for "*.local"
   Traffic Policy TLS Mode: DISABLE
…

$ istioctl x describe pod -n nf2nf-nginx $(oc get pod -l app=nginx -n nf2nf-nginx -o jsonpath={.items..metadata.name})   

Pod: nginx-76c4c9fd97-gc4wz
…
--------------------
Effective PeerAuthentication:
   Workload mTLS mode: STRICT
Applied PeerAuthentication:
   default.istio-system, default.nf2nf-nginx, nginx-svc-policy.nf2nf-nginx

```

Edit the PeerAuthentication policy to PERMISSIVE mode:

```

$ oc edit peerauthentication nginx-svc-policy -n nf2nf-nginx
…
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: nginx-svc-policy
  namespace: nf2nf-nginx
spec:
  mtls:
    mode: PERMISSIVE
  selector:
    matchLabels:
      app: nginx

Send a test request:
$ oc exec $(oc get pod -n nf2nf-h2load -l app=h2load -o jsonpath={.items..metadata.name}) -c h2load -n nf2nf-h2load -- curl  -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200” response code, as the “DIABLE” mode in DR would only work with “PERMISSIVE” mode PA policy.

```

Inspect the mTLS settings in the Consumer and Producer sidecars, set by workload level TLS policy:


```

$ istioctl x describe pod -n nf2nf-h2load $(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})
…
Pod: h2load-784d9dc5d9-9xhqg
…
--------------------
Service: h2load
   Port: http2 80/HTTP2 targets pod port 80
DestinationRule: default for "*.local"
   Traffic Policy TLS Mode: DISABLE
…

$ istioctl x describe pod -n nf2nf-nginx $(oc get pod -l app=nginx -n nf2nf-nginx -o jsonpath={.items..metadata.name})   

Pod: nginx-76c4c9fd97-gc4wz
…
--------------------
Effective PeerAuthentication:
   Workload mTLS mode: PERMISSIVE
Applied PeerAuthentication:
   default.istio-system, default.nf2nf-nginx, nginx-svc-policy.nf2nf-nginx

```
