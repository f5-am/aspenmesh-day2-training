Lab 16.1: Aspen Mesh Packet Inspector I HTTP Filter
The APMI1 HTTP filter can be applied globally in istio-system or CNF namespaces, with a selector for all the SBI workloads with a common label in a cluster/a namespace.
1.	Check the AMPI1 filter settings:


```

$ cd ~/am-training/samples/packet-inspector-1-filters
$ vi values.yaml
…
# Please see the envoy documentation above for more matching options, which includes:
# - header key presence, header value matching (prefix, contains, regex, etc.)
# - path matching options (exact, prefix, suffix, contains, regex, etc.)
# - option to invert match definitions
# To match requests to endpoint "/status*" AND header key "abcd"
# OR to match requests to endpoint "/get":
match_criteria:
# - url_match:
#     (https://www.envoyproxy.io/docs/envoy/latest/api-v3/type/matcher/v3/path.proto)
#      path:
#        prefix: "/status"
#   (https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/route/v3/route_components.proto#config-route-v3-headermatcher)
- header_match:
    name: **"host"**
    present_match: **true**
  url_match:
    path:
      exact: **"/"**

# To enable tapping on a deployment with pod template label "app=nginx"
# workloadSelector: {}
workloadSelector:
  labels:
    app: nginx

# Used to enabled HTTP traffic capture on the cluster.
httpCapture:
  enabled: true

# Used to enable Diameter traffic capture on the cluster.
diameterCapture:
  enabled: false
  port: 3868
…
Modify the definition if needed and save change if any.


```
