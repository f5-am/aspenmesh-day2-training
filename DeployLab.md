### Follow the steps below to bring up the lab. 
#### Note: These steps can be completed prior to training, right after you receive the email invitation as described below.
1. Access the link to lab. 
   ```
   You will receive an email from courses@notify.udf.f5.com
   In the email click on the link similar to below
   You can log into the UDF here:
   https://account.f5.com/api/invite/udf/user/cHJhYmhhdGthcmtpQHlhaG9vLmNvbQ== 
   ```
2. Cicking the link will take you to the page to create F5 UDF account. Provide the Name password etc to create account.
   ![create udf account](images/create_udf_account.png){width=50% and length=50%}


3. You will receive another email to Activate your account.
   ![activate udf account](images/active_udf_account.png){width=50% and length=50%}

4. Click on Activate account. This will take you to the UDF page. Click on the Invited User Option
   ![invited users](images/invited_users.png){width=50% and length=50%}

5. This will take you to 2-Step authentication process. Click on the email Option. and seletc "Send me the Code"
   ![two step](images/2_step_authentication.png){width=50% and length=50%}

6. An email will be sent with a 6 digit code. Apply the code in the verification box and click Finish
   ![verification finish](images/verification_finish.png){width=50% and length=50%}

7. In the next page check the agreement box and continue
   ![agreement](images/agreee_and_continue.png){width=50% and length=50%}

   #### The steps below can be started an hour before the training

8. In the next window, the course will be listed. Click on Launch and then Join
   
   ![course list](images/Course_list.png){width=50% and length=50%}
   
   ![joing](images/Join.png){width=50% and length=50%}

9. Now select the Deployment tab. You should see all the nodes such as masters workers nodes being started.
   
   ![deployment](images/deployment_tab.png){width=50% and length=50%}

10. Once all the nodes are up and green, click Access on the "OCP provisioner" node and select XRDP/1440X900 to access the provisioner/bastion node by RDP.

   ![rdp](images/ocp_provisioner_access_via_RDP.png){width=50% and length=50%}
   
   This will open the RDP client. You can also use Console or SSH (mac) or webconsole options. But RDP is recommended.

11. One the desktop click on Activities on top left and terminal icon will appear on menu. Select the terminal to open it.

   ![rdp2](images/rdp.png){width=50% and length=50%}
   ![rdp3](images/rdp_terminal.png){width=50% and length=50%}

12. If you use webconsole to acccess the bastion, it may log you as root. So change the user to cloud-user
```
su - cloud-user
```
13. Check the cluster nodes by running the followng command.
```
oc get nodes
```
If the output shows all nodes are in Ready State, your cluster is ready for the lab. If you don't get any output or if the command hangs, CTRL C and perform the steps 14 and 15 below.

14. Switch to recovery mode
```
oc config use-context default/api-ocp-f5-udf-com:6443/recovery
```
15. Run the following command until all nodes are in Ready state. And then use CTRL C to exit out. 
```
while date ; do
oc get nodes
oc get csr --no-headers | grep Pending | awk '{print $1}' | xargs --no-run-if-empty oc  adm certificate approve
sleep 5
done

```
15. Now your cluster is ready
