In this lab exercise we will look at enabling, configuring, and using Authorization policies.

### Introduction

The Authorization policy can be defined in global mesh level in the root namespace istio-system, or applied to any CNF Producer namespace, or to specific CNF Producer workloads with selector in any CNF Producer namespaces. The Producer workloads sidecars (ordinary sidecar or Istio ingress gateway) perform Authorization on received requests from Consumers according to the Authorization policy (s).

Reference: (https://istio.io/latest/docs/reference/config/security/authorization-policy/)

An Istio Request Authorization Policy typically includes a selector, an action, a list of rules and a provider:
- The selector specifies the target of the policy.
- The action specifies whether to allow or deny the request.
- The rules specify when to trigger the action.


![Intro](images/10c_Picture1.png){width=50% and length=50%}





1. An Istio Authorization Policy Rule typically includes a from, a to, and when:
- The from field in the rules specifies the sources of the request.
- The to field in the rules specifies the operations of the request.
- The when field specifies the conditions needed to apply the rule.


Istio Authorization Policy Rules match requests from a list of sources that perform a list of operations subject to a list of conditions. A match occurs when at least one source, one operation and all conditions match the request. An empty rule is always matched.

The table in the following shows the definable sources in the from field of a rule, which could be ANDed.


![Intro_2](images/10c_Picture2.png)

####Note: The namespace can be from any local/remote clusters if no conflict in names.

Any string field in the rule supports Exact, Prefix, Suffix and Presence match:
- Exact match: exact string match.
- Prefix match: a string with an ending "*". For example, "test.abc.*" matches "test.abc.com", "test.abc.com.cn”. 
- Suffix match: a string with a starting "*". For example, "*.abc.com" matches "eng.abc.com", "test.eng.abc.com".
- Presence match: “*” will match when value is not empty.


Istio Authorization Policy Rule to field includes a list of operations to be performed subject to a list of conditions. Operation specifies the operations of a request. 

The following table shows the definable operations in the to field of a rule, which could be ANDed.

![Intro_3](images/10c_Picture3.png)

The paths can be normalized in different ways defined by mesh global parameter, according to RFC 3986: NONE, BASE (default), MERGE_SLASHES, DECODE_AND_MERGE_SLASHES.

Note: This modifies the actual path in the requests towards CNF apps.


The following table shows the definable conditions in the when field of a rule, which could be ANDed:


![Intro_4](images/10c_Picture4.png)


The supported keys and value formats can be used in conditions referencing the following table:
Reference: https://istio.io/latest/docs/reference/config/security/conditions/

![Intro_5](images/10c_Picture5.png)








2. Istio Authorization Policy supports ALLOW, DENY and CUSTOM actions. Multiple policies can be applied, each with a different action, as needed to secure access to the workloads. 

Istio Authorization Policy also supports the AUDIT action to decide whether to log requests, which needs plugin for implementation.



Istio checks for matching policies in layers, in this order: CUSTOM, DENY, and then ALLOW. 
For each type of action, Istio first checks if there is a policy with the action applied, and then checks if the request matches the policy’s specification. If a request doesn’t match a policy in one of the layers, the check continues to the next layer.

For workloads without authorization policies applied, Istio allows all requests.


3. Istio Authorization Policy scope (target) is determined by metadata/namespace and an optional workload selector:
- The metadata/namespace tells which namespace the policy applies. If set to root namespace, the policy applies to all namespaces in a mesh.
- The workload selector can be used to further restrict where a policy applies.




Workload selector specifies the criteria used to determine if a policy can be applied to a proxy. The matching criteria includes the metadata associated with a proxy, workload instance info such as labels attached to the pod/VM, or any other info that the proxy provides to Istio during the initial handshake. If multiple conditions are specified, all conditions need to match for the workload instance to be selected. 

Currently, only label-based selection mechanism is supported. The selector uses labels to select the target workload. The selector contains a list of {key: value} pairs, where the key is the name of the label. If not set, the authorization policy applies to all workloads in the same namespace as the authorization policy.

Notes/Recommendations
Reference: https://istio.io/latest/docs/ops/best-practices/security/#authorization-policies

- SPIFFE IDs from workload certificates can be used as source principals in AuthZ Policy conveniently, as which is the default URI in SAN field of all workload certificates. Prerequisite: Ensure the namespaces and ServiceAccount combination for each CNF service being unique across all clusters.

- Multiple AuthZ policies could be defined at namespace or workload level, the ACTION and the context of the policies determine the outcome of the policies, not necessarily the least specific overwrites the more specific policies, or vise versa.

- When using XFCC header in when condition with DNS fields, recommend to “globing” the DNS names instead of adding a list of actual FQDNs if possible. For instance, the smf-001.5gc.vzims.com, smf-002.5gc.vzims.com, …smf-00x.5gc.vzims.com could be combined into a single smf.5gc.vzims.com, to reduce the DNS fields added in SAN and number of AuthZ policies.

- Mutual TLS connection is required to have Consumer CNF certificate passed to the Producer CNFs, and for the certificate content or identities from the Consumer certificate populated into XFCC header by sidecar. If the Consumer doesn’t have sidecar injected, but its certificate contains SPIFFE ID/DNS/URI in SAN, AuthZ policy or CNF code logics would still be able to use these identities as filters. Reference XFCC mode recommendations for different use cases.



## XFCC Modes

#### Reference: https://my.aspenmesh.io/client/docs/1.11/operations/configure-xfcc-header
	
- Aspen Mesh allows to configure XFCC behavior only for ingress gateway workloads. By default, Aspen Mesh does not assume that the outside caller is a trusted entity, so the XFCC header is SANITIZE_SET which resets the XFCC header to the immediate client’s certificate. The content of the client certificate (in pem format) is also populated in the XFCC header by default.

- When the proxy is deployed as a sidecar the Aspen Mesh XFCC configuration is set to APPEND_FORWARD by default globally. Sidecar proxies (both consumer and producer) will add the client’s certificate (acquired via mTLS) to the XFCC header before sending the request to the next hop. It is feasible to change XFCC header mode for virtual listeners on ports OUTBOUN connections and/or INBOUND connections with EnvoyFilters for the selected CNF workload. XFCC configuration is applied once the connection is trusted. The Consumer certificate content (in pem format) is not by default populated in the XFCC header, which could also be enabled with EnvoyFilter. 


#### Note: Certificate content can only be enabled when the XFCC mode is APPEND_FORWARD or SANITIZE_SET.


## Use Cases & Labs

Verify Sample Consumer and Producer apps are UP and running:

### Note: Please reach out to the instructor if the Consumer and Producer apps are not running 
```

Verify the results:
$ oc get all -n nf2nf-h2load
$ oc get all -n nf2nf-nginx

Send a test request:
$ oc exec $(oc get pod -n nf2nf-h2load -l app=h2load -o jsonpath={.items..metadata.name}) -c h2load -n nf2nf-h2load -- curl  -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200” response code.

```

#### Notes:

1. This will create sample CNFs (h2load, sleep, nginx, httpbin, etc) in different namespaces (nf2nf-h2load, nf2nf-nginx, nf2nf-sleep) if they do not exist. The CNF Consumer and Producer must have sidecar injected for this lab. The CNF Consumer can be without sidecar injected, as long as the Consumer application handles the mTLS properly.
2. The sample CNF Consumer service in nf2nf-h2load namespace will request “nginx.nf2nf-nginx.svc.cluster.local” service in nf2nf-nginx namespace in above example, both CNF Consumer and Producer services are within the same mesh without  Gateways in the path to egress and ingress the traffic.
3. The <test-case> command line parameter needs to follow certain format: "4", "6", and "d", which indicates if the CNF Consumer or Producer Services will be single stack (ipv4 or ipv6) or dual stack mode. 
The "s" indicates sidecar injection enabled. The above sample "4s-ds" means CNF Consumer is a single stack ipv6 service and the CNF Producer is a dual stack ipv6 preferred service. The script will trigger the configure changes (ipFamilies and ipFamilyPolicy) to the "Service" object according to this <test-case> value for different scenarios.
4. The <install-flag> can be “-I” for install or “-U” for uninstall the services.


### Lab 10c.1: Enable Authorization Policy at Namespace Level

This example demonstrates Authorization policy with a pair of Consumer and Producer services at namespace level. The namespace can also be the istio-system, which would be a mesh level policy. Workload selector can be defined to filter a list of Producer workloads in the namespace the AuthZ policy being enabled.

When a workload context ALLOW AuthZ policy is created in a namespace, there will be a deny-all AuthZ policy created implicitly in the namespace level also, customized namespace wide deny-all or allow-all policy could be created explicitly to overwrite the default, as illustrated in this lab.

1.	Verify the Authorization policy definition for Producer “nginx” workload in a Producer namespace:


```

$ cd ~/am-training/samples/generic-authz-scenarios
$ vi values.yaml
…
authz_namespace:
  enabled: true
  action: DENY
  selector:
    enabled: true
    labels:
      app: nginx
…
authz_basics:
  enabled: false
…
xfcc_mode_reset:
  enabled: false
…

Modify the “authz_namespace” definition if needed, set the “authz_basics” and “xfcc_mode_reset” sections “enabled” to “false”, and save change if any. 

```


#### Notes:
There are 3 sections: “authz_namespace”, “authz_basics”, and “xfcc_mode_reset” in the values.yaml file, each with an “enabled” flag to turn on/off its corresponding configurations in the labs. 1~3 sections can be enabled together as needed:
- authz_namespace: If enabled, a namespace level AuthZ policy will be created with parameters in this section. The “selector.enabled” if set to “true” would allow the policy to be applied only to the workloads with label “app: nginx” in above  example, otherwise the policy will apply to all the workloads in the nsamespace. The “action” can be ALLOW or DENY as needed.
- authz_basics: If enabled, a more granular AuthZ policy will be created, and overwrites the namespace level policy. See later lab for details.
- xfcc_mode_reset: If enabled, will overwrite the default XFCC mode in Producer sidecar. See later labs for details.


2.	Create the namespace level AuthZ policy:

```

$ cd ~/am-training/samples/generic-authz-scenarios
$ ./setup.sh <producer-ns> <install-flag>

For example:
$ ./setup.sh nf2nf-nginx -I

Verify the namespace level authZ policy created:
$ oc get authorizationpolicy -n nf2nf-nginx
$ oc get authorizationpolicy authz-policy-ns -n nf2nf-nginx -o yaml
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: authz-policy-ns
  namespace: nf2nf-nginx
spec:
  action: DENY
  rules:
  - {}
  selector:
    matchLabels:
      app: nginx

```

#### Notes:
(a) The setup.sh script "install-flag" command line parameter can be "-I" for creating the AuthZ policy (s) to the Producer namespace, while the "-U" will delete the policy (s) from the Producer namespace
(c) The seup.sh script "producer-ns" command line parameter will only be the destination CNF namespace which authorizes the CNF Consumer incoming requests with defined AuthZ policy (s). 

3.	Verify if the AuthZ policy authorizes and denied the request:

```

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

The result should expect “403”:

* Connected to nginx.nf2nf-nginx.svc.cluster.local (10.121.45.110) port 80 (#0)
* Using HTTP2, server supports multiplexing
* Connection state changed (HTTP/2 confirmed)
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Using Stream ID: 1 (easy handle 0x56519d1ace80)
> GET / HTTP/2
> Host: nginx.nf2nf-nginx.svc.cluster.local
> user-agent: curl/7.81.0
> accept: */*
>
* Connection state changed (MAX_CONCURRENT_STREAMS == 2147483647)!
< HTTP/2 403
< content-length: 19
< content-type: text/plain
< date: Sun, 22 Oct 2023 17:24:13 GMT
< server: envoy
…
{"title":"Forbidden","detail":"istio-envoy-nf2nf-nginx: Pod: nginx-6577696c6d-sn6l5 Dest Authority Header: nginx.nf2nf-nginx.svc.cluster.local RBAC: access denied: F5ASM--,rbac_access_denied_matched_policy[ns[nf2nf-nginx]-policy[authz-policy-ns]-rule[0]]","status":403}

```

4.	Verify other Producer “httpbin” in the same namespace is NOT impacted by the AuthZ policy, as the AuthZ policy only applied to workload “nginx”:

```

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/get -4

The “httpbin” service is in the same namespace with “nginx”, the result should expect a “200” code:

* Connected to httpbin.nf2nf-nginx.svc.cluster.local (10.121.43.162) port 8000 (#0)
> GET /get HTTP/1.1
> Host: httpbin.nf2nf-nginx.svc.cluster.local:8000
> User-Agent: curl/7.81.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< server: gunicorn/19.9.0
< date: Sun, 22 Oct 2023 18:57:29 GMT
< content-type: application/json
< content-length: 713
< access-control-allow-origin: *
< access-control-allow-credentials: true
< x-envoy-upstream-service-time: 46
<
{ [71{
  "args": {},
  "headers": {
    "Accept": "*/*",
    "Host": "httpbin.nf2nf-nginx.svc.cluster.local:8000",
    "User-Agent": "curl/7.81.0",
    "X-B3-Parentspanid": "9e296a580cb9dc08",
    "X-B3-Sampled": "0",
    "X-B3-Spanid": "cb872deb4fda93d7",
    "X-B3-Traceid": "4aebf3665e2f11fd9e296a580cb9dc08",
    "X-Envoy-Attempt-Count": "1",
    "X-Forwarded-Client-Cert": "By=spiffe://cluster.local/ns/nf2nf-nginx/sa/httpbin;Hash=efe069a2ce7a2b0d73291b528d34f870434d1f260b0c26a6a726523bf27f5509;Subject=\"\";URI=spiffe://cluster.local/ns/nf2nf-h2load/sa/h2load;DNS=h2load.nf2nf-h2load.svc.cluster.local"

```


### Lab 10c.2: Enable Authorization Policy with Granular Conditions

This example demonstrates AuthZ Policy defined with granular parameters and conditions.

1.	Verify the Authorization policy definition for Producer “nginx” workload in a Producer namespace:


```

$ cd ~/am-training/samples/generic-authz-scenarios
$ vi values.yaml
…
authz_namespace:
  enabled: false
…
authz_basics:
  enabled: true
  action: ALLOW
  selector:
    enabled: false
    labels:
      app: nginx
  rules:
    from:
      principals:
      - "cluster.local/ns/nf2nf-h2load/sa/h2load"
      notPrincipals:
      - "cluster.local/ns/nf2nf-sleep/sa/sleep"
      namespaces:
      - "nf2nf-h2load"
      notNamespaces:
      - "nf2nf-sleep"
    to:
      operation:
        hosts:
        - "nginx.nf2nf-nginx.*"
        notHosts:
        - "httpbin.nf2nf-nginx.*"
        ports:
        - "80"
        notPorts:
        - "8000"
        methods:
        - "GET"
        notMethods:
        - "PATCH"
        paths:
        - "/"
        - "/get"
        notPaths:
        - "/headers"
    when:
      key: "request.headers[X-Forwarded-Client-Cert]"
      values:
      - "*DNS=h2load.nf2nf-h2load.svc.cluster.local*"
      - "*DNS=sleep.nf2nf-h2load.svc.cluster.local*"
      notValues:
      - "*DNS=sleep.nf2nf-sleep.svc.cluster.local"
…
xfcc_mode_reset:
  enabled: false
…

Modify the “authz_basics” definition if needed, set the “authz_namespace” and “xfcc_mode_reset” sections “enabled” to “false”, and save change if any.

```

### Notes: 
(a)	The “authz_basics” parameters in values.yaml don’t include all the parameters for the from, to and when, users can replace or modify the definition as needed for additional labs.
(b)	When “authz_basics” is enabled, as its action is ALLOW, a namespace level DENY policy will be automatically created.


2.	Create a granular AuthZ policy:

```
$ cd ~/am-training/samples/generic-authz-scenarios
$ ./setup.sh <producer-ns> <install-flag>

For example:
$ ./setup.sh nf2nf-nginx -I

Verify the AuthZ policy created:
$ oc get authorizationpolicy -n nf2nf-nginx
$ oc edit authorizationpolicy authz-policy-basics -n nf2nf-nginx
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  annotations:
    authz.contains.aspenmesh.io/xfcc: "true"
  name: anthz-policy-basics
  namespace: nf2nf-nginx
spec:
  action: ALLOW
  rules:
  - from:
    - source:
        principals:
        - cluster.local/ns/nf2nf-h2load/sa/h2load
    - source:
        notPrincipals:
        - cluster.local/ns/nf2nf-h2load/sa/sleep
    - source:
        namespaces:
        - nf2nf-sleep
        - nf2nf-h2load
    to:
    - operation:
        hosts:
        - nginx.nf2nf-nginx.*
        methods:
        - GET
        notHosts:
        - httpbin.nf2nf-nginx.*
        notMethods:
        - PATCH
        notPaths:
        - /headers
        notPorts:
        - "8000"
        paths:
        - /
        - /get
        ports:
        - "80"
    when:
    - key: request.headers[X-Forwarded-Client-Cert]
      notValues:
      - '*DNS=sleep.nf2nf-sleep.svc.cluster.local'
      values:
      - '*DNS=h2load.nf2nf-h2load.svc.cluster.local'
      - '*DNS=sleep.nf2nf-h2load.svc.cluster.local'

```

3.	Verify the SANs of the Consumers TLS certificates:

```
$ istioctl proxy-config secret h2load-b57d4ffd9-m2dm8 -n nf2nf-h2load -o json | jq '.dynamicActiveSecrets[0].secret.tlsCertificate.certificateChain.inlineBytes' -r | base64 -d | openssl x509 -noout -text
…
   X509v3 Subject Alternative Name: critical
      DNS:h2load.nf2nf-h2load.svc.cluster.local, URI:spiffe://cluster.local/ns/nf2nf-h2load/sa/h2load 
…

$ istioctl proxy-config secret sleep-75948454c7-d6khw -n nf2nf-h2load -o json | jq '.dynamicActiveSecrets[0].secret.tlsCertificate.certificateChain.inlineBytes' -r | base64 -d | openssl x509 -noout -text
…
   X509v3 Subject Alternative Name: critical
      DNS:sleep.nf2nf-h2load.svc.cluster.local, URI:spiffe://cluster.local/ns/nf2nf-h2load/sa/sleep
…

$ istioctl proxy-config secret sleep-94dcc98b6-fv7w4 -n nf2nf-sleep -o json | jq '.dynamicActiveSecrets[0].secret.tlsCertificate.certificateChain.inlineBytes' -r | base64 -d | openssl x509 -noout -text 
…
   X509v3 Subject Alternative Name: critical
      DNS:sleep.nf2nf-sleep.svc.cluster.local, URI:spiffe://cluster.local/ns/nf2nf-sleep/sa/sleep
…

```

4.	Verify the AuthZ policy can allow/deny requests as expected:
a.	Edit the AuthZ policy to only allow Consumer “h2load” accessing Producer “nginx”. Other Consumers accessing to Producer “nginx” service should be blocked. Also no Consumer should be able to access Producer service “httpbin”.


```
$ oc edit authorizationpolicy -n nf2nf-nginx
Remove some parameters from the “rules” configuration, just leave the following:
…
spec:
  action: ALLOW
  rules:
  - from:
    - source:
        principals:
        - cluster.local/ns/nf2nf-h2load/sa/h2load
    to:
    - operation:
        hosts:
        - nginx.nf2nf-nginx.svc.cluster.local
        notHosts:
        - httpbin.nf2nf-nginx.svc.cluster.local:8000


Run the test cases:
$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200” code, as this matched both “principals” and “hosts” lists.

$ oc exec "$(oc get pod -l app=sleep -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “403” code, as the “sleep” identity is not in the allowed “principals” list. 

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/headers

Should expect “403” code, as the “httpbin.nf2nf-nginx.svc.cluster.local:8000” is in the “notHosts” list.

$ oc exec "$(oc get pod -l app=sleep -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/headers

Should expect “403” code, as this is blocked by both “principals” and “noHosts” lists.

```

b.	Edit the AuthZ policy to allow other Consumers excepts “h2load” accessing Producer “httpbin” service only, the Producer “nginx” is not allowed to access. Also the path in the requests to “httpbin” can only be “/get”, the requests to “/headers” path is blocked.

```
$ cd ~/am-training/samples/generic-authz-scenarios
$ ./setup.sh <producer-ns> <install-flag>
Refresh the AuthZ policy:
$ ./setup.sh nf2nf-nginx -U
$ ./setup.sh nf2nf-nginx -I

$ oc edit authorizationpolicy -n nf2nf-nginx
Modify some parameters from the “rules” configuration, just leave the following:
…
spec:
  action: ALLOW
  rules:
  - from:
    - source:
        notPrincipals:
        - cluster.local/ns/nf2nf-h2load/sa/h2load
    to:
    - operation:
        hosts:
        - httpbin.nf2nf-nginx.svc.cluster.local:8000
        notHosts:
        - nginx.nf2nf-nginx.svc.cluster.local
        notPaths:
        - /headers
        paths:
        - /get
…
$ oc exec "$(oc get pod -l app=sleep -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/get

Should expect “200” code, as the “hosts” list and “paths” are matched.

$ oc exec "$(oc get pod -l app=sleep -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/headers

Should expect “403” code, as the “notPaths” blocked the “/headers” path.

$ oc exec "$(oc get pod -l app=sleep -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/get

Should expect “403”code, as the “nginx” service matched to the “notHosts” list.

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/get

Should expect “403” code, as the “h2load” matched the “notPrincipals”.

$ oc exec "$(oc get pod -l app=sleep -n nf2nf-sleep -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-sleep -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/get

Should expect “200” code, although the request initiated from a different Consumer.

$ oc exec "$(oc get pod -l app=sleep -n nf2nf-sleep -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-sleep -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/get

Should expect “403” code, although the request initiated from a different Consumer.

```

c.	Edit the AuthZ policy to apply only to Producer “nginx” workload:

```

$ cd ~/am-training/samples/generic-authz-scenarios
$ ./setup.sh <producer-ns> <install-flag>
Refresh the AuthZ policy:
$ ./setup.sh nf2nf-nginx -U
$ ./setup.sh nf2nf-nginx -I
$ oc edit authorizationpolicy -n nf2nf-nginx
Modify some parameters from the “rules” configuration, just leave the following:
…
spec:
  action: ALLOW
  rules:
  - from:
    - source:
        principals:
        - cluster.local/ns/nf2nf-h2load/sa/h2load
    - source:
        notPrincipals:
        - cluster.local/ns/nf2nf-h2load/sa/sleep
    to:
    - operation:
        hosts:
        - nginx.nf2nf-nginx.svc.cluster.local
        notHosts:
        - httpbin.nf2nf-nginx.svc.cluster.local:8000
        paths:
        - /
        - /get
  selector:
    matchLabels:
      app: nginx
…
$ oc exec "$(oc get pod -l app=sleep -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “403” code, as the Consumer “sleep” matched the “notPrincipals” list.

$ oc exec "$(oc get pod -l app=sleep -n nf2nf-sleep -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-sleep -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200” code, as this “sleep” is not blocked by the “notPrincipals”.

$  oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “200”code, as the “h2load” is in “principals” list.

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/get

Should expect “200” code, as the AuthZ policy doesn’t apply to Producer “httpbin” workload.

Send requests from other Consumers (sleep.nf2nf-h2load, sleep.nf2nf-sleep) to Producer “httpbin”, should all expect “200” code as the policy doesn’t apply to it.

```

d.	Edit the AuthZ policy with “when” condition filtering XFCC headers in Consumers requests:

```
$ cd ~/am-training/samples/generic-authz-scenarios
$ ./setup.sh <producer-ns> <install-flag>
Refresh the AuthZ policy:
$ ./setup.sh nf2nf-nginx -U
$ ./setup.sh nf2nf-nginx -I
$ oc edit authorizationpolicy -n nf2nf-nginx
Modify some parameters from the “rules” configuration, just leave the following:
…
spec:
  action: ALLOW
  rules:
  - to:
    - operation:
        hosts:
        - httpbin.nf2nf-nginx.svc.cluster.local:8000
        notPaths:
        - /headers
        paths:
        - /get
    when:
    - key: request.headers[X-Forwarded-Client-Cert]
      values:
      - '*DNS=h2load.nf2nf-h2load.svc.cluster.local'
      - '*DNS=sleep.nf2nf-h2load.svc.cluster.local'
…
$ oc exec "$(oc get pod -l app=sleep -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/get

Should expect “200” code, as the Consumer “sleep” with “DNS=sleep.nf2nf-h2load.svc.cluster.local” in XFCC header matched to the “when” condition.

$ oc exec "$(oc get pod -l app=h2load -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c h2load -n nf2nf-h2load -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/get

Should expect “200” code, as the Consumer “h2load” with “DNS=h2load.nf2nf-h2load.svc.cluster.local” in XFCC header matched to the “when” condition.

Repeat above 2 tests with “/headers” path, should expect “403” code, as blocked by  “notPaths”

$ oc exec "$(oc get pod -l app=sleep -n nf2nf-sleep -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-sleep -- curl -vvv http://httpbin.nf2nf-nginx.svc.cluster.local:8000/get

Should expect “403”code, as the “DNS=sleep.nf2nf-sleep.svc.cluster.local” in XFCC header NOT matched to the “when” condition.

$  oc exec "$(oc get pod -l app=sleep -n nf2nf-h2load -o jsonpath={.items..metadata.name})" -c sleep -n nf2nf-h2load -- curl -vvv --http2-prior-knowledge http://nginx.nf2nf-nginx.svc.cluster.local:80/

Should expect “403” code, as the AuthZ policy only allows requests to Producer “httpbin” workload.

Send requests from other Consumers (h2load.nf2nf-h2load, sleep.nf2nf-sleep) to Producer “nginx”, should all expect “403” code as the policy doesn’t allow to it.

```


### Lab 10c.3: XFCC Mode Customization

This lab shows how to modify the default XFCC mode on sidecar configurations.

1.	Verify the settings: 

```
$ cd ~/am-training/samples/generic-authz-scenarios
$ vi values.yaml
…
xfcc_mode_reset:
  enabled: true
  filter:
    context: SIDECAR_INBOUND
    xfccMode: FORWARD_ONLY
    certMode: false
  selector:
    enabled: true
    labels:
      app: nginx


Verify the XFCC mode on Producer sidecars:
$ istioctl pc listeners nginx-7986fd7cbc-888th.nf2nf-nginx -o json | grep forwardClientCertDetails
                 "forwardClientCertDetails": "APPEND_FORWARD"
…
$ istioctl pc listeners httpbin-69ddb64bcd-8d4wl.nf2nf-nginx -o json | grep forwardClientCertDetails
                 "forwardClientCertDetails": "APPEND_FORWARD",

```

2.	Overwrite the XFCC mode for specific workload:

```

$ cd ~/am-training/samples/generic-authz-scenarios
$ ./setup.sh <producer-ns> <install-flag>

For example:
$ ./setup.sh nf2nf-nginx -I

Verify the EnvoyFilter created:

Verify the XFCC mode after the overwrite:
$ istioctl pc listeners nginx-7986fd7cbc-888th.nf2nf-nginx -o json | grep forwardClientCertDetails
               "forwardClientCertDetails": "FORWARD_ONLY",
…
$ istioctl pc listeners httpbin-69ddb64bcd-8d4wl.nf2nf-nginx -o json | grep forwardClientCertDetails
               "forwardClientCertDetails": "APPEND_FORWARD",
…

As the EnvoyFilter is only applied to “nginx” workload, no impact to the “httpbin” workload.

```

```

Verify the EnvoyFilter created:
$ oc get envoyfilter xfcc-reset-filter -n nf2nf-nginx -o yaml
apiVersion: networking.istio.io/v1alpha3
kind: EnvoyFilter
metadata:
  name: xfcc-reset-filter
  namespace: nf2nf-nginx
spec:
  configPatches:
  - applyTo: NETWORK_FILTER
    match:
      context: SIDECAR_INBOUND
      listener:
        filterChain:
          filter:
            name: envoy.filters.network.http_connection_manager
    patch:
      operation: MERGE
      value:
        typed_config:
          '@type': type.googleapis.com/envoy.extensions.filters.network.http_connection_manager.v3.HttpConnectionManager
          forward_client_cert_details: FORWARD_ONLY
          max_request_headers_kb: 86
          set_current_client_cert_details:
            cert: false
            dns: true
            uri: true
  workloadSelector:
    labels:
      app: nginx

```

Note: The “FORWARD_ONLY” XFCC mode will only forward the XFCC header received from Consumer request to Producer app “nginx”, without inserting XFCC header. As the Consumer app and sidecar didn’t insert any XFCC header either in above example, the effect with this “FORWARD_ONLY” mode will result in no XGCC header in the request to Producer “nginx”.

